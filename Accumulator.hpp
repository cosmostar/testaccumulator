/* 
 * File:   Acuumulator.h
 * Author: lemz
 *
 * Created on January 28, 2016, 6:14 PM
 */
#ifndef ACkUMULATOR_H  
#define	ACkUMULATOR_H 


#ifdef DEBUG
#define DEBUG_ACC_UC1BI974WP1MSMY5 DEBUG
#endif

#include <vector>
#include <deque>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/timer.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/concept_check.hpp>

#ifndef WHERE_UC1BI974WP1MSMY5 
#define WHERE_UC1BI974WP1MSMY5 std::cout << __FILE__ << ":"<< __LINE__ << " " <<__func__; 
#endif 

// Terminator
void Log_Recursive_UC1BI974WP1MSMY5(const char* file, int line, std::ostringstream& msg)
{
    std::cout << file << "(" << line << "): " << msg.str() << std::endl;
}

// "Recursive" variadic function
template<typename T, typename... Args>
void Log_Recursive_UC1BI974WP1MSMY5(const char* file, int line, std::ostringstream& msg, 
                   T value, const Args&... args)
{
    msg << value;
    Log_Recursive_UC1BI974WP1MSMY5(file, line, msg, args...);
}

// Log_Recursive wrapper that creates the ostringstream
template<typename... Args>
void LogWrapper_UC1BI974WP1MSMY5(const char* file, int line, const Args&... args)
{
    std::ostringstream msg;
    Log_Recursive_UC1BI974WP1MSMY5(file, line, msg, args...);
}





#ifndef LOG_UC1BI974WP1MSMY5
#if DEBUG_ACC_UC1BI974WP1MSMY5
#define LOG_UC1BI974WP1MSMY5(...) LogWrapper_UC1BI974WP1MSMY5(__FILE__, __LINE__, __VA_ARGS__)
#else
#define LOG_UC1BI974WP1MSMY5(...)
#endif
#endif



template <class T, size_t acc_size, int timeout_ms> class Accumulator {
public:

    Accumulator();
    virtual ~Accumulator();
    Accumulator(const Accumulator& orig) = delete;

    int push_data(void * data, size_t len);
    T get_last(void);

    int get_count();

private:
    boost::timer m_tmr;
    boost::mutex m_mutex;

    boost::circular_buffer <unsigned char> m_acc;
    std::deque <T> m_data;
};

template <class T, size_t acc_size, int timeout_ms>
Accumulator<T, acc_size, timeout_ms>::Accumulator() : m_acc(acc_size) {
}

template <class T, size_t acc_size, int timeout_ms>
Accumulator<T, acc_size, timeout_ms>::~Accumulator() {
}

template <class T, size_t acc_size, int timeout_ms>
int Accumulator<T, acc_size, timeout_ms>::push_data(void * data, size_t len) {
    boost::unique_lock<boost::mutex> lock(this->m_mutex);

	LOG_UC1BI974WP1MSMY5 ("invoked buffer size is ",m_acc.size());

    if (this->m_tmr.elapsed() > timeout_ms / 1000.0) {
		LOG_UC1BI974WP1MSMY5 ("warning net proto timeouted discrad previous content");
        this->m_acc.clear();

        for (int i = 0; i < len; i++) {

            LOG_UC1BI974WP1MSMY5 (" invoked buffer size is ", m_acc.size());
            m_acc.push_back(((unsigned char *) data)[i]);
        }

    } else {
        LOG_UC1BI974WP1MSMY5 (" invoked buffer size is ", m_acc.size());

        for (int i = 0; i < len; i++) {
            LOG_UC1BI974WP1MSMY5 ( " invoked buffer size is ", m_acc.size());
            m_acc.push_back(((unsigned char *) data)[i]);
        }

        LOG_UC1BI974WP1MSMY5 ( "data added to previous content current buffer size is ", m_acc.size());
    }
    m_tmr.restart();


    bool is_object_detected;
    size_t awaited_size;
    while (m_acc.size()) {
        awaited_size = T::get_awaited_size(m_acc.linearize(), m_acc.size(), is_object_detected);
        LOG_UC1BI974WP1MSMY5("try to extract awaited size : object detected is ", is_object_detected,
		" awaited size is %lu, ", awaited_size, 
		" actual size is %lu, ", len,  
		" buffer size is %lu ", m_acc.size());
   if (is_object_detected) {

            if (m_acc.size() >= awaited_size) {
                std::vector<unsigned char> vec;

                for (int i = 0; i < awaited_size; i++) {
                    vec.push_back(m_acc.front());
                    m_acc.pop_front();
                }

                T object(vec.data(), vec.size() * sizeof (unsigned char));

                this->m_data.push_back(object);
                LOG_UC1BI974WP1MSMY5(" object detected; data size is ", m_acc.size());
				
            } else {
                LOG_UC1BI974WP1MSMY5(" wait for other; data size is ", m_acc.size());
                break;
            }
        } else {
            break;
            //this->m_acc.clear();
		LOG_UC1BI974WP1MSMY5(" ");
        }
    }
    return this->m_data.size();
}

template <class T, size_t acc_size, int timeout_ms>
T Accumulator<T, acc_size, timeout_ms>::get_last(void) {
    boost::unique_lock<boost::mutex> lock(this->m_mutex);

    T object;
    object = m_data.front();
    this->m_data.pop_front();
    return object;

}


template <class T,size_t acc_size, int timeout_ms>
int Accumulator<T, acc_size, timeout_ms>::get_count() {
	boost::unique_lock<boost::mutex> lock(this->m_mutex);
	LOG_UC1BI974WP1MSMY5(" size is ",m_data.size());
	return m_data.size();
}

template <class T>
struct Accumulator_ready_checker : public T {
public:

    BOOST_CONCEPT_USAGE(Accumulator_ready_checker) {
        const void * data;
        size_t sz;
        T ins (data,sz);
    }

private:
    T t;
};

#endif	/* ACUUMULATOR_H */
